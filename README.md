# README #

### What is this repository for? ###

* Quick summary
This repo is for anyone who wants to extend or get HR recruitment support web application which is open-sourced

* Version
Initial version 0.0.1, will require lots of work on front-end and back-end fixes, updates and little mods.


### How do I get set up? ###

* Summary of set up
runs on Java 7, PostgreSQL 9.1, Spring 4.1.6, Spring Security 3.2.7 and AngularJS 1.3.15

* Configuration
actually, it requires none

* Dependencies
requires java jdk1.7 and tomcat7. additionaly PostgreSQL database

* Database configuration can be found in \HRApp\src\main\tomcat\conf\context.xml

* Deployment instructions
mvn clean install tomcat7:run


### Who do I talk to? ###

* Repo owner and admin
via mail: blemiec@gmail.com